import { TestBed, inject } from '@angular/core/testing';

import { AppService } from './app.service';
import { EditMember, Member } from './modal/member-modal'
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('AppService', () => {
  let httpTestControl: HttpTestingController;
  let appService: AppService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AppService],
      imports: [HttpClientModule,HttpClientTestingModule]
    });
  });

  beforeEach(()=>{
    appService = TestBed.get(AppService);
    httpTestControl = TestBed.get(HttpTestingController);
  })

  afterEach(()=>{
    httpTestControl.verify()
  })

  it('should test get members method',()=>{
    let testMember: EditMember[] = [
      {id:1, firstName:'xxx', lastName:'yyy',jobTitle:'racer',team:'xxx',status:'Active'},
      {id:2, firstName:'xxx', lastName:'yyy',jobTitle:'racer',team:'xxx',status:'Active'}
    ] 
    appService.getMembers().subscribe((members)=>{
      expect(testMember).toBe(members,'should check mocked data')
    });
    
    const req = httpTestControl.expectOne(appService.api + '/members');
    expect(req.cancelled).toBeFalsy();
    expect(req.request.responseType).toEqual('json');
    req.flush(testMember);   
    
  })

  it('should test get team method',()=>{
    let testTeams = [
      {id:1, teamName:'xxx'},
      {id:2, teamName:'xxx'}
    ] 
    appService.getTeams().subscribe((team)=>{
      expect(testTeams).toBe(team,'should check mocked data')
    });
    
    const req = httpTestControl.expectOne(appService.api + '/teams');
    expect(req.cancelled).toBeFalsy();
    expect(req.request.responseType).toEqual('json');
    req.flush(testTeams);   
    
  })

  it('should test add member post method',()=>{
    let newMember: Member = {firstName:'xxx', lastName:'yyy',jobTitle:'racer',team:'xxx',status:'Active'}

    appService.addMember(newMember).subscribe((addedMember)=>{
      expect(addedMember).toBe(newMember);
    });

    const req = httpTestControl.expectOne(appService.apiPost);
    expect(req.cancelled).toBeFalsy();
    expect(req.request.responseType).toEqual('json');
    req.flush(newMember);   
  })

  it('should be created', inject([AppService], (service: AppService) => {
    expect(service).toBeTruthy();
  }));
});
