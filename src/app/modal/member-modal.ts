export interface Member {
    firstName: string;
    lastName: string;
    jobTitle: string;
    team: string;
    status: string;
  }

  export interface EditMember {
    id: number;
    firstName: string;
    lastName: string;
    jobTitle: string;
    team: string;
    status: string;
  }