import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';

import { MembersComponent } from './members.component';

import { Router } from '@angular/router';
import { AppService } from '../app.service'
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { of } from 'rxjs';
import { BrowserModule,By } from '@angular/platform-browser';

const mockMembersList  = [
  {id:1, firstName:'xxx', lastName:'yyy',jobTitle:'racer',team:'xxx',status:'Active'},
  {id:2, firstName:'xxx', lastName:'yyy',jobTitle:'racer',team:'xxx',status:'Active'}
] 

describe('MembersComponent', () => {

  let component: MembersComponent;
  let fixture: ComponentFixture<MembersComponent>;
  let mockMembers = mockMembersList;
  let appService: AppService;
  let router = {
    navigate: jasmine.createSpy('navigate')
  }

  beforeEach(async(() => {
    window.history.pushState({message:''},'','')
    TestBed.configureTestingModule({
      declarations: [MembersComponent],
      imports: [HttpClientModule, RouterModule],
      providers: [AppService,
        {
          provide: Router,useValue: router ,
          useClass: class {
            navigate = jasmine.createSpy('navigate');
          }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MembersComponent);
    component = fixture.componentInstance;
    appService = TestBed.get(AppService);
    fixture.detectChanges();
  });

  it('should test get members in onInit', fakeAsync(()=>{
    let memberSpy = spyOn(appService,'getMembers').and.returnValue(of(mockMembers));
    let subSpy = spyOn(appService.getMembers(),'subscribe');
    component.ngOnInit();
    tick();
    expect(memberSpy).toHaveBeenCalledBefore(subSpy);
    expect(subSpy).toHaveBeenCalled();
    expect(component.members).toBeDefined();
  }))

  it('Route for add member',()=>{
    let btn = fixture.debugElement.query(By.css('#addMemberButton'))
    component.goToAddMemberForm();
    expect(router.navigate).toHaveBeenCalledWith(['/addmember']); 
    
  });

  it('Route for edit member',()=>{
    let btn = fixture.debugElement.query(By.css('#editMemberButton'))
    component.editMemberByID(mockMembers[0]);
    expect(router.navigate).toHaveBeenCalledWith(['/addmember'],Object({ state: Object({ memberDetail: mockMembers[0] }) })); 
    
  });

  it('delete member by ID',fakeAsync(()=>{
    let mockDeleteId = 2
    let memberSpy = spyOn(appService,'deleteMember').and.returnValue(of(mockMembers));
    let subSpy = spyOn(appService.deleteMember(mockDeleteId),'subscribe');
    component.onConfirm();
    tick();
    expect(memberSpy).toHaveBeenCalledBefore(subSpy);
    expect(subSpy).toHaveBeenCalled();
  }))
  
});
