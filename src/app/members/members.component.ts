import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AppService } from '../app.service';
import {MessageService} from 'primeng/api';
import { Router } from '@angular/router';
import { Member } from '../modal/member-modal';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css'],
  providers: [MessageService]
})
export class MembersComponent implements OnInit, AfterViewInit {
  members = [];
  message = null
  deleteID = 0
  constructor(public appService: AppService, private router: Router,private messageService: MessageService) {}

  ngOnInit() {   

    this.appService.getMembers().subscribe(members => (this.members = members));
  }

  ngAfterViewInit() {
    this.message = history.state.message 
    console.log(this.message,history.state)
    if(this.message) {
      this.messageService.add({severity:'success', summary: 'Success Message', detail:this.message});
    }
  }

  goToAddMemberForm() {
    this.router.navigate(['/addmember'])
  }

  editMemberByID(member: Member) {
    console.log(member)
    this.router.navigate(['/addmember'],{state:{memberDetail:member}})
  }

  deleteMemberById(id: number) {
    console.log('inside del',id)
    this.deleteID = id
    this.messageService.clear();
    this.messageService.add({key: 'c', sticky: true, severity:'warn', summary:'Are you sure?', detail:'Confirm to delete'});
    
  }

  onConfirm() {
    this.appService.deleteMember(this.deleteID).subscribe(res => {
      this.messageService.add({severity:'success', summary: 'Success Message', detail:'Member Deleted'});
      this.appService.getMembers().subscribe(members => (this.members = members));
    })
    this.messageService.clear('c');
  }

  onReject() {
    this.messageService.clear('c');
  }
}
