import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { MembersComponent } from '../members/members.component'
import { HttpClient } from '@angular/common/http';
import { BrowserModule,By } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { RouterModule } from '@angular/router';
import { Router } from '@angular/router';
import { from } from 'rxjs';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let router = {
    navigate: jasmine.createSpy('navigate')
  }
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent,MembersComponent],
      imports: [ReactiveFormsModule, RouterModule, HttpClientModule],
      providers: [
        {
          provide: Router,useValue: router ,
          useClass: class {
            navigate = jasmine.createSpy('navigate');
          }
        },
        HttpClient
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    
  });

  it('login form should be invalid if formcontrol has error',()=>{
    component.loginForm.controls['username'].setValue('xx');
    component.loginForm.controls['password'].setValue('');
    expect(component.loginForm.invalid).toBeTruthy();
    fixture.detectChanges();
    let btn = fixture.debugElement.query(By.css('button'))
    expect(btn.nativeElement.disabled).toBeTruthy();
  });

  it('login form should be valid if formcontrol has valid values',()=>{
    component.loginForm.controls['username'].setValue('xx');
    component.loginForm.controls['password'].setValue('yyy');
    expect(component.loginForm.invalid).toBeFalsy();
    fixture.detectChanges();
    let btn = fixture.debugElement.query(By.css('button'))
    console.log(btn)
    expect(btn.nativeElement.disabled).toBeFalsy();
    fixture.detectChanges();
    component.login();
    expect(router.navigate).toHaveBeenCalledWith(['/members']); 
    
  });

  

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
