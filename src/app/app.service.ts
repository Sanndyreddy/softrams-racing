import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  api = 'http://localhost:8000/api';
  apiPost = 'http://localhost:3000/members';
  username: string;

  constructor(private http: HttpClient ) {}

  // Returns all members
  getMembers() {
    return this.http
      .get(`${this.api}/members`)
      .pipe(catchError(this.handleError));
  }

  setUsername(name: string): void {
    this.username = name;
  }

  addMember(memberForm) {
    let header = new HttpHeaders();
    header.append('Content-Type', 'application/json');
    header.append('Accept' , 'application/json');

    return this.http.post(this.apiPost,memberForm, {headers :header })
    .pipe(catchError(this.handleError));
  }

  editMember(memberForm,id) {
    let header = new HttpHeaders();
    header.append('Content-Type', 'application/json');
    header.append('Accept' , 'application/json');

    return this.http.put(this.apiPost+"/"+id,memberForm, {headers :header })
    .pipe(catchError(this.handleError));
  }

  deleteMember(memberId) {
    return this.http.delete(this.apiPost+'/'+memberId)
    .pipe(catchError(this.handleError));
  }

  getTeams() {
    return this.http
      .get(`${this.api}/teams`)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    return [];
  }
}
