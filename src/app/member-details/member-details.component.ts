import { Component, OnInit, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormControl } from '@angular/forms';
import { AppService } from '../app.service';
import { Router } from '@angular/router';
import { Member } from '../modal/member-modal';



@Component({
  selector: 'app-member-details',
  templateUrl: './member-details.component.html',
  styleUrls: ['./member-details.component.css']
})
export class MemberDetailsComponent implements OnInit, OnChanges {
  memberModel: Member;
  editMemberId: 0
  memberForm: FormGroup;
  submitted = false;
  editFalg = false;
  memberFormHeader = '';
  alertType: String;
  alertMessage: String;
  teams = [];

  constructor(private fb: FormBuilder, private appService: AppService, private router: Router) {}

  ngOnInit() {
    this.memberFormHeader = 'Add Member to Racing Team';
    this.memberForm=new FormGroup({
      firstName: new FormControl(''),
      lastName: new FormControl(''),
      jobTitle: new FormControl(''),
      team: new FormControl(''),
      status: new FormControl('',Validators.required)
      
    });
    this.memberModel = history.state.memberDetail
        console.log(history.state.memberDetail)
    this.appService.getTeams().subscribe(teams =>{
      this.teams = teams
      console.log('teams',this.teams)
    })

    if (this.memberModel) {
      console.log('inside if member edit')
      this.editMemberId = history.state.memberDetail.id
      this.memberForm.get('firstName').setValue(this.memberModel.firstName)
    this.memberForm.get('lastName').setValue(this.memberModel.lastName)
    this.memberForm.get('jobTitle').setValue(this.memberModel.jobTitle)
    this.memberForm.get('team').setValue(this.memberModel.team)
    this.memberForm.get('status').setValue(this.memberModel.status)
    this.editFalg = true;
    this.memberFormHeader = 'Edit Member to Racing Team';
    } else {
      this.editFalg = false;
      this.memberFormHeader = 'Add Member to Racing Team';
    }
  }

  ngOnChanges() {}

  // TODO: Add member to members
  onSubmit(form: FormGroup) {
    this.submitted=true;
    console.log('form',form)
    this.memberModel = form.value;
    console.log(this.memberModel);
    if(this.editFalg) {
      this.appService.editMember(this.memberModel,this.editMemberId).subscribe(response=>
        {          console.log(response);        
          this.router.navigate(['/members'],{state:{message:'Member Saved'}})
        })

    } else {
      this.appService.addMember(this.memberModel).subscribe(response=>
        {
          console.log(response);        
          this.router.navigate(['/members'],{state:{message:'Member Added'}})
        })
    }
    
  }

  navigateToMembers() {
    this.router.navigate(['/members'])
  }
}
