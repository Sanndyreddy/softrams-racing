import { fakeAsync,async, ComponentFixture, TestBed, tick } from '@angular/core/testing';
import {DebugElement, Component}from '@angular/core';
import { MemberDetailsComponent } from './member-details.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AppService } from '../app.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserModule,By } from '@angular/platform-browser';
import { of } from 'rxjs'

const teamMembers = [
  {id:1,teamName:'xxx'},
  {id:2,teamName:'xxx'}
]
// Bonus points!
describe('MemberDetailsComponent', () => {
  let component: MemberDetailsComponent;
  let fixture: ComponentFixture<MemberDetailsComponent>;
  let appService: AppService;
  let mockTeam = teamMembers;
  let router = {
    navigate: jasmine.createSpy('navigate')
  }
  beforeEach(async(() => {
    window.history.pushState({memberDetail:''},'')
    TestBed.configureTestingModule({
      declarations: [MemberDetailsComponent],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        RouterModule,
        RouterTestingModule
      ],
      providers: [
        HttpClient,
        FormBuilder,
        {
          provide: Router,useValue: router ,AppService,
          useClass: class {
            navigate = jasmine.createSpy('navigate');
          }
        }
      ]
    }).compileComponents().then(()=>{
      fixture = TestBed.createComponent(MemberDetailsComponent);
      component = fixture.componentInstance;
      appService = TestBed.get(AppService);
      fixture.detectChanges();
    });
  }));

  it('should test get teams in onInit', fakeAsync(()=>{
    let memberSpy = spyOn(appService,'getTeams').and.returnValue(of(mockTeam));
    let subSpy = spyOn(appService.getTeams(),'subscribe');
    component.ngOnInit();
    tick();
    expect(memberSpy).toHaveBeenCalledBefore(subSpy);
    expect(subSpy).toHaveBeenCalled();
    expect(component.teams).toBeDefined();
  }))

  it('Route for back to member summary page',()=>{
    
    component.navigateToMembers();
    expect(router.navigate).toHaveBeenCalledWith(['/members']); 
    
  });

  it('form should be invalid if input is invalid',()=>{
    let btn = fixture.debugElement.query(By.css('#submitButton'))
    expect(btn.nativeElement.disabled).toBeTruthy();
  })

  it('add form should be valid if input is valid',()=>{
    component.memberForm.controls['firstName'].setValue('xxx');
    component.memberForm.controls['lastName'].setValue('yyy');
    component.memberForm.controls['jobTitle'].setValue('zzz');
    component.memberForm.controls['team'].setValue('Formula 2 - Car 63');
    component.memberForm.controls['status'].setValue('Active');
    fixture.detectChanges();
    let memberAddSpy = spyOn(appService,'addMember').and.returnValue(of(mockTeam));
    let subSpy = spyOn(appService.addMember(component.memberForm),'subscribe');
    let btn = fixture.debugElement.query(By.css('#submitButton'))
    expect(btn.nativeElement.disabled).toBeFalsy();
    component.onSubmit(component.memberForm)  
    component.editFalg = false;
    expect(memberAddSpy).toHaveBeenCalledBefore(subSpy);
    expect(subSpy).toHaveBeenCalled();
    })

    it('edit form should be valid if input is valid',()=>{
      component.memberForm.controls['firstName'].setValue('xxx');
      component.memberForm.controls['lastName'].setValue('yyy');
      component.memberForm.controls['jobTitle'].setValue('zzz');
      component.memberForm.controls['team'].setValue('Formula 2 - Car 63');
      component.memberForm.controls['status'].setValue('Active');
      fixture.detectChanges();
      let memberAddSpy = spyOn(appService,'addMember').and.returnValue(of(mockTeam));
      let subSpy = spyOn(appService.addMember(component.memberForm),'subscribe');
      let btn = fixture.debugElement.query(By.css('#submitButton'))
      expect(btn.nativeElement.disabled).toBeFalsy();
      component.onSubmit(component.memberForm)  
      component.editFalg = true;
      expect(memberAddSpy).toHaveBeenCalledBefore(subSpy);
      expect(subSpy).toHaveBeenCalled();
      })

      it('form should be valid initially if it is navigate from edit button',fakeAsync(()=>{
        let memberSpy = spyOn(appService,'getTeams').and.returnValue(of(mockTeam));
        let subSpy = spyOn(appService.getTeams(),'subscribe');
        let mockmemberModel = {firstName:'xxx',lastName:'yyy',jobTitle:'zzz',team:'Formula 2 - Car 63',status:'Active'}
        window.history.pushState({memberDetail:mockmemberModel},'')
        component.ngOnInit();
        tick();
        fixture.detectChanges();
        console.log(component.editFalg)
        expect(component.editFalg).toBeTruthy();
        expect(component.memberForm.valid).toBeTruthy();
      }));

      it('form should be invalid initially if it is navigate from add member button',fakeAsync(()=>{
        let memberSpy = spyOn(appService,'getTeams').and.returnValue(of(mockTeam));
        let subSpy = spyOn(appService.getTeams(),'subscribe');
        component.ngOnInit();
        tick();
        fixture.detectChanges();
        console.log(component.editFalg)
        expect(component.editFalg).toBeFalsy();
        expect(component.memberForm.valid).toBeFalsy();
      }));
  
});